package com.example.karina_1202160041_si4001_pab_modul4;

public class Menu {
    private int id;
    private String imageMenu;
    private String inputNamaMenu;
    private String inputHarga;
    private String inputDeskripsi;

    public Menu (String imageMenu, String inputNamaMenu, String inputHarga, String inputDeskripsi) {
        this.imageMenu = imageMenu;
        this.inputNamaMenu = inputNamaMenu;
        this.inputHarga = inputHarga;
        this.inputDeskripsi = inputDeskripsi;
    }

    public int getId() {
        return id;
    }

    public String getImageMenu(){
        return imageMenu;
    }

    public String getInputNamaMenu(){
        return inputNamaMenu;
    }

    public String getInputHarga() {
        return inputHarga;
    }

    public String getInputDeskripsi() {
        return inputDeskripsi;
    }
}
