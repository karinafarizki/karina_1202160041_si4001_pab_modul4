package com.example.karina_1202160041_si4001_pab_modul4;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import java.io.ByteArrayInputStream;
import java.util.ArrayList;

public class Adapter extends RecyclerView.Adapter<Adapter.ViewHolder> {

    private ArrayList<Menu> daftarMenu;
    private Context mContext;

    public Adapter(ArrayList<Menu> daftarMenu, Context mContext) {
        this.daftarMenu = daftarMenu;
        this.mContext = mContext;
    }

    @Override
    public Adapter.ViewHolder onCreateViewHolder( ViewGroup viewGroup, int i) {
        return new Adapter.ViewHolder(LayoutInflater.from(mContext).inflate(R.layout.menu_list, viewGroup, false)) {
        };
    }

    @Override
    public void onBindViewHolder( Adapter.ViewHolder viewHolder, int i) {
        Menu menu = daftarMenu.get(i);
        viewHolder.bindTo(menu);

    }

    @Override
    public int getItemCount() {
        return daftarMenu.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private TextView nama, harga;
        private ImageView image;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            nama = itemView.findViewById(R.id.namaMenu);
            harga = itemView.findViewById(R.id.hargaMenu);
            image = itemView.findViewById(R.id.profile);


            itemView.setOnClickListener(this);
        }

        @SuppressLint("StaticFieldLeak")
        public void bindTo(final Menu menu) {
            nama.setText(menu.getInputNamaMenu());
            harga.setText(menu.getInputHarga());

            final StorageReference islandRef = FirebaseStorage.getInstance().getReference().child("images/" + menu.getImageMenu());

            final long ONE_MEGABYTE = 10* 1024 * 1024;
            islandRef.getBytes(ONE_MEGABYTE).addOnSuccessListener(new OnSuccessListener<byte[]>() {
                @Override
                public void onSuccess(byte[] bytes) {
                    Drawable d = Drawable.createFromStream(new ByteArrayInputStream(bytes), null);
                    image.setImageDrawable(d);
                }
            }).addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception exception) {
                    image.setImageResource(R.drawable.ic_launcher_background);
                }
            });
        }

        @Override
        public void onClick(View view) {
            Toast.makeText(mContext, nama.getText().toString(), Toast.LENGTH_SHORT).show();
        }

        }
    }
