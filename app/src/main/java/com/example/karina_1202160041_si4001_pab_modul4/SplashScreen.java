package com.example.karina_1202160041_si4001_pab_modul4;

import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class SplashScreen extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);

        getSupportActionBar().hide();

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                Intent theIntent =  new Intent(SplashScreen.this, Login.class);
                SplashScreen.this.startActivity(theIntent);
                SplashScreen.this.finish();
            }
        },2000);
    }
}
