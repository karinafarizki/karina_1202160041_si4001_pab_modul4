package com.example.karina_1202160041_si4001_pab_modul4;

import android.content.Intent;
import android.os.AsyncTask;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;

public class Login extends AppCompatActivity {

    private EditText email, password;
    private FirebaseAuth auth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        email = findViewById(R.id.inputEmail);
        password = findViewById(R.id.inputPassword);

        //Authenthication Firebase
        auth = FirebaseAuth.getInstance();
    }

    public boolean check(){
        if (email.getText().toString().equals("")){
            email.setError("Anda Belum Memasukkan Email!");
            email.requestFocus();
            return false;
        }
        if (password.getText().toString().equals("")){
            password.setError("Anda Belum Memasukkan Password!");
            password.requestFocus();
            return false;
        }
        return true;
    }

        public void daftarAkun (View view){
            Intent inten = new Intent(Login.this, Register.class);
            startActivity(inten);
        }

    public void masuk(View view) {
        if (check()) {
            new AsyncTask<Void,Void,Boolean>(){
                @Override
                protected Boolean doInBackground(Void... voids) {
                    auth.signInWithEmailAndPassword(email.getText().toString(), password.getText().toString())
                            .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                                @Override
                                public void onComplete(@NonNull Task<AuthResult> task) {
                                    if (task.isSuccessful()) {
                                        startActivity(new Intent(Login.this, MainActivity.class));
                                        finish();
                                    } else {
                                        Toast.makeText(Login.this, task.getException().getMessage(), Toast.LENGTH_SHORT).show();
                                    }
                                }
                            });
                    return null;
                }

            }.execute();
        }
    }
}
